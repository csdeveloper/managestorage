﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ManageStorage.DataLayer.Interfaces;
using ManageStorage.DataLayer.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ManageStorage.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportController : ControllerBase
    {
        public readonly IReportRepository _reportRepository;
        public ReportController (IReportRepository reportRepository)
        {
            _reportRepository = reportRepository;
        }
        [HttpGet("[action]")]
        public IActionResult ReportAll()
        {
            try
            {
                List<Report> reports = _reportRepository.ReportAll();
                return Ok(new { result = reports });
            }
            catch (Exception e)
            {

                return BadRequest(e);
            }
        }
        [HttpPost("[action]")]
        public IActionResult ReportByPagination(Pagination Pagination)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                var reports = _reportRepository.ReportByPagination(Pagination);
                return Ok(new { result = reports });
            }
            catch (Exception e)
            {

                return BadRequest(e);
            }
        }
    }
}