﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ManageStorage.DataLayer.Interfaces;
using ManageStorage.DataLayer.ViewModels;

namespace ManageStorage.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RecordController : ControllerBase
    {
        public readonly IRecordRepository _recordRepo;
        public RecordController(IRecordRepository recordRepo){
            _recordRepo = recordRepo;
        }

        [HttpPost("[action]")]
        public IActionResult AddCommodity([FromBody] CommodityToAdd Commodity)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                _recordRepo.AddCommodity(Commodity);
                return Ok(new { message = "Commodity added successfuly!" });
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
        [HttpPost("[action]")]
        public IActionResult AddCategory([FromBody] CategoryToAdd Category)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                _recordRepo.AddCategory(Category);
                return Ok(new { message = "Category added successfuly!" });
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
        [HttpPost("[action]")]
        public IActionResult AddEntrance ([FromBody] EntranceToAdd Entrance)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                _recordRepo.AddEntrance(Entrance);
                return Ok(new { message = "Entrance added successfuly!" });
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
        [HttpPost("[action]")]
        public IActionResult AddSellRecord ([FromBody] SellRecordToAdd SellRecord)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                _recordRepo.AddSellRecord(SellRecord);
                return Ok(new { message = "Sell record added successfuly" });
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
        [HttpGet("[action]")]
        public IActionResult GetCategories ()
        {
            try
            {
                List<CategoryToShow> Categories = _recordRepo.GetCategories();
                return Ok(new { result = Categories });
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
    }
}