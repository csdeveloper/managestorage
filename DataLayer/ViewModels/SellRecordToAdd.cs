﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ManageStorage.DataLayer.ViewModels
{
    public class SellRecordToAdd
    {
        [Required]
        public string CustomerName { get; set; }
        [Required]
        public string CommodityId { get; set; }
        [Required]
        public int Count { get; set; }
        [Required]
        public string Price { get; set; }
    }
}
