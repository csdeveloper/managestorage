﻿using ManageStorage.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManageStorage.DataLayer.ViewModels
{
    public class OrderByInputs
    {
        public ReportProperties Title { get; set; }
        public bool IsDescending { get; set; }
    }
    public class Pagination
    {
        public int page { get; set; }
        public int count { get; set; }
    }
    public class ReportFilterInputs
    {
        public OrderByInputs OrderBy { get; set; }
        public string Filter { get; set; }
        public Pagination Paging { get; set; }
    }

}
