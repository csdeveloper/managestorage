﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ManageStorage.DataLayer.ViewModels
{
    public class CategoryToAdd
    {
        [Required]
        public string Title { get; set; }
    }
}
