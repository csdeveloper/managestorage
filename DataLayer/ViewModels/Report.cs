﻿using ManageStorage.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManageStorage.DataLayer.ViewModels
{
    public class Report
    {
        public string CommodityId { get; set; }
        public string CommodityName { get; set; }
        public string CategoryName { get; set; }
        public int Stock { get; set; }
        public string State { get; set; }
    }
}
