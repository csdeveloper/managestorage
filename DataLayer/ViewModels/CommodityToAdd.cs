﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ManageStorage.DataLayer.ViewModels
{
    public class CommodityToAdd
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public int MinimumStock { get; set; }
        [Required]
        public string CategoryId { get; set; }
    }
}
