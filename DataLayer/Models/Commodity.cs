﻿using ManageStorage.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ManageStorage.DataLayer.Models
{
    public class Commodity
    {
        public string Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public int MinimumStock { get; set; }
        [Required]
        public string CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        public Category Category { get; set; }
        public CommodityState State { get; set; }
        public List<Entrance> Entrances { get; set; }
        public List<SellRecord> SellRecords { get; set; }
    }
}
