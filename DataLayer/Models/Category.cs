﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ManageStorage.DataLayer.Models
{
    public class Category
    {
        public string Id { get; set; }
        public List<Commodity> Commodities { get; set; }
        [Required]
        public string Title { get; set; }
    }
}
