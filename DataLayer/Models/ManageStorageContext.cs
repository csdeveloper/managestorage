﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManageStorage.DataLayer.Models
{
    public class ManageStorageContext:DbContext
    {
        public ManageStorageContext(DbContextOptions<ManageStorageContext> options)
            : base(options)
        { 
        }
        public DbSet<Commodity> Commodities { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<AccountingDocument> AccountingDocuments { get; set; }
        public DbSet<Entrance> Entrances { get; set; }
        public DbSet<SellRecord> SellRecords { get; set; }

    }
}
