﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ManageStorage.DataLayer.Models
{
    public class Entrance
    {
        public string Id { get; set; }
        [Required]
        public int Count { get; set; }
        [Required]
        public string EntryDate { get; set; }
        [Required]
        public string CommodityId { get; set; }
        [ForeignKey("CommodityId")]
        public Commodity Commodity { get; set; }
    }
}
