﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ManageStorage.DataLayer.Models
{
    public class SellRecord
    {
        public string Id { get; set; }
        [Required]
        public string CustomerName { get; set; }
        [Required]
        public string SellDate { get; set; }
        [Required]
        public string CommodityId { get; set; }
        [ForeignKey("CommodityId")]
        public Commodity Commodity { get; set; }
        [Required]
        public int Count { get; set; }
        [Required]
        public string Price { get; set; }
    }
}
