﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ManageStorage.DataLayer.Models
{
    public class AccountingDocument
    {
        public string Id { get; set; }
        [Required]
        public string RegistrationDate { get; set; }
        [Required]
        public string SellRecordId { get; set; }
        [ForeignKey("SellRecordId")]
        public SellRecord SellRecord { get; set; }
        public string Price { get; set; }
    }
}
