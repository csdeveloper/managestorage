﻿using ManageStorage.DataLayer.Interfaces;
using ManageStorage.DataLayer.Models;
using ManageStorage.DataLayer.ViewModels;
using ManageStorage.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManageStorage.DataLayer.Repositories
{
    public class RecordRepository : IRecordRepository
    {
        private readonly ManageStorageContext _dbContext;
        public RecordRepository(ManageStorageContext dbContext)
        {
            _dbContext = dbContext;
        }
        public void AddCategory(CategoryToAdd Category)
        {
            Category foundedCategory = _dbContext.Categories.FirstOrDefault(category => category.Title == Category.Title);
            if(foundedCategory!=null)
            {
                throw new Exception("already exist!");
            }
            Category categoryToAdd = new Category { Title = Category.Title };
            try
            {
                _dbContext.Categories.Add(categoryToAdd);
                _dbContext.SaveChanges();
            }
            catch
            {
                throw new Exception("Error in adding category!");
            }
        }

        public void AddCommodity(CommodityToAdd Commodity)
        {
            Category selectedCategory = _dbContext.Categories.Include(cat => cat.Commodities).FirstOrDefault(category => category.Id == Commodity.CategoryId);
            if (selectedCategory == null)
            {
                throw new Exception("category not Exist!");
            }
            Commodity foundedCommadityInSelectedCategory = selectedCategory.Commodities.FirstOrDefault(commodity => commodity.Title == Commodity.Title);
            if(foundedCommadityInSelectedCategory != null)
            {
                throw new Exception("commodity with same title exist in selected category!");
            }
            try
            {
                Commodity commodityToAdd = new Commodity
                {
                    CategoryId = Commodity.CategoryId,
                    MinimumStock = Commodity.MinimumStock,
                    Title = Commodity.Title,
                    State = Commodity.MinimumStock == 0 ? CommodityState.Unavailable : CommodityState.ReadyToOrder
                };
                _dbContext.Commodities.Add(commodityToAdd);
                _dbContext.SaveChanges();
            }
            catch
            {
                throw new Exception("error in adding commodity!");
            }
        }

        public void AddEntrance(EntranceToAdd Entrance)
        {
            Commodity foundedCommodity = _dbContext.Commodities.FirstOrDefault(commodity => commodity.Id == Entrance.CommodityId);
            if(foundedCommodity == null)
            {
                throw new Exception("commodity not exist!");
            }
            Entrance entranceToAdd = new Entrance
            {
                CommodityId = Entrance.CommodityId,
                Count = Entrance.Count,
                EntryDate = DateTool.GetTimeInUnix().ToString()
            };
            try
            {
                _dbContext.Entrances.Add(entranceToAdd);

                foundedCommodity.MinimumStock += Entrance.Count;
                foundedCommodity.State = CommodityState.ReadyToOrder;

                _dbContext.SaveChanges();
            }
            catch
            {
                throw new Exception("error in adding entrance");
            }
        }

        public void AddSellRecord(SellRecordToAdd SellRecord)
        {
            Commodity foundedCommodity = _dbContext.Commodities.FirstOrDefault(commodity => commodity.Id == SellRecord.CommodityId);
            if(foundedCommodity == null)
            {
                throw new Exception("commodity not exist!");
            }
            if (foundedCommodity.MinimumStock < SellRecord.Count)
            {
                throw new Exception("count of commodity for sell should not be more than minimum stock of it!");
            }
            SellRecord sellRecordToAdd = new SellRecord
            {
                CommodityId = SellRecord.CommodityId,
                Count = SellRecord.Count,
                CustomerName = SellRecord.CustomerName,
                Price = SellRecord.Price,
                SellDate = DateTool.GetTimeInUnix().ToString()
            };
            try
            {
                _dbContext.SellRecords.Add(sellRecordToAdd);
                
                foundedCommodity.MinimumStock -= SellRecord.Count;
                foundedCommodity.State = foundedCommodity.MinimumStock == 0 ? CommodityState.Unavailable : CommodityState.ReadyToOrder;
                
                _dbContext.SaveChanges();

                _AddAccountingDocument(SellRecord.Price, sellRecordToAdd.Id);
            }
            catch
            {

                throw new Exception("error in adding sell record");
            }
        }

        public List<CategoryToShow> GetCategories()
        {
            try
            {
                List<CategoryToShow> categories = _dbContext.Categories.Select(category => new CategoryToShow
                {
                    Title = category.Title,
                    Id = category.Id
                }).ToList();
                return categories;
            }
            catch(Exception e)
            {
                throw e;
            }
        }

        private void _AddAccountingDocument (string Price , string SellRecordId)
        {
            AccountingDocument accountingDocumentToAdd = new AccountingDocument
            {
                Price = Price,
                RegistrationDate = DateTool.GetTimeInUnix().ToString(),
                SellRecordId = SellRecordId
            };
            try
            {
                _dbContext.AccountingDocuments.Add(accountingDocumentToAdd);
                _dbContext.SaveChanges();
            }
            catch
            {
                throw new Exception("error in adding account document!");
            }
        }
    }
}
