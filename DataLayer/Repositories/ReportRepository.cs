﻿using ManageStorage.DataLayer.Interfaces;
using ManageStorage.DataLayer.Models;
using ManageStorage.DataLayer.ViewModels;
using ManageStorage.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManageStorage.DataLayer.Repositories
{
    public class ReportRepository : IReportRepository
    {
        private readonly ManageStorageContext _dbContext;
        public ReportRepository (ManageStorageContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<Report> ReportAll()
        {
            try
            {
                List<Report> reports = _dbContext.Commodities
                .Include(com => com.Category.Title)
                .Select(commodity => new Report
                {
                    CategoryName = commodity.Category.Title,
                    CommodityId = commodity.Id,
                    CommodityName = commodity.Title,
                    Stock = commodity.MinimumStock,
                    State = commodity.State == CommodityState.ReadyToOrder ? "آماده سفارش":"نا موجود"
                }).ToList();
                return reports;
            }
            catch
            {
                throw new Exception("error occurred in reporting!");
            }
            
        }

        public List<Report> ReportByPagination(Pagination Pagination)
        {
            try
            {
                int skip = Pagination.count * (Pagination.page-1);
                int take = Pagination.count;
                List<Report> reports = _dbContext.Commodities
                .Include(com => com.Category.Title)
                .Select(commodity => new Report
                {
                    CategoryName = commodity.Category.Title,
                    CommodityId = commodity.Id,
                    CommodityName = commodity.Title,
                    Stock = commodity.MinimumStock,
                    State = commodity.State == CommodityState.ReadyToOrder ? "آماده سفارش" : "نا موجود"
                }).Skip(skip).Take(take).ToList();
                return reports;
            }
            catch
            {
                throw new Exception("error occurred in reporting!");
            }
        }

    }
}
