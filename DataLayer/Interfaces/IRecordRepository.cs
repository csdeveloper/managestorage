﻿using ManageStorage.DataLayer.Models;
using ManageStorage.DataLayer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManageStorage.DataLayer.Interfaces
{
    public interface IRecordRepository
    {
        void AddCommodity(CommodityToAdd Commodity);
        void AddCategory(CategoryToAdd Category);
        List<CategoryToShow> GetCategories();
        void AddEntrance(EntranceToAdd Entrance);
        void AddSellRecord(SellRecordToAdd SellRecord);
    }
}
