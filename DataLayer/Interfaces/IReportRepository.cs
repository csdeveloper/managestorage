﻿using ManageStorage.DataLayer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManageStorage.DataLayer.Interfaces
{
    public interface IReportRepository
    {
        List<Report> ReportAll();
        List<Report> ReportByPagination(Pagination Pagination);
    }
}
