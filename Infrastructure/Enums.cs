﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManageStorage.Infrastructure
{
    public enum CommodityState
    {
        Unavailable,
        ReadyToOrder
    }

    public enum ReportProperties
    {
        CommodityId=0,
        CommodityName=1,
        CategoryName=2,
        Stock=3,
        State=4
    }
}
