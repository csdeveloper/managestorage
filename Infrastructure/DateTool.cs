﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace ManageStorage.Infrastructure
{
    public class DateTool
    {
        public static Int32 GetTimeInUnix()
        {
            Int32 unixTime = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            return (unixTime);
        }
        public static string UnixTimeStampToPersianDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            PersianCalendar pc = new PersianCalendar();
            string persianDate = string.Format("{0}/{1}/{2}", pc.GetYear(dtDateTime), pc.GetMonth(dtDateTime), pc.GetDayOfMonth(dtDateTime));
            return persianDate;
        }
    }
}
